import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import './App.scss';
import { PrivateRoute } from './Shared/PrivateRoute';
import { authTokenAccess } from './store/actions/auth';
import { blogStep1Data, blogStep2Data, blogStep3Data } from './store/actions/blogData';
import { connect } from 'react-redux';
import data from './Configs/data.json';
import Login from './Widget/LoginView/Login';
import Dashboard from './Widget/DashboardView/Dashboard';
import NotFound from './Component/UX-NotFound/NotFound';
const configStrings = data;

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			authToken: null
		};
	}
	resetStateData = () => {
		return new Promise((resolve, reject) => {
			try {
				const authToken = sessionStorage.getItem('auth-token');
				const step1Data = sessionStorage.getItem('step1');
				const step2Data = sessionStorage.getItem('step2');
				const step3Data = sessionStorage.getItem('step3');
				console.log('yes', step1Data);
				if (authToken) {
					this.props.setAauthToken(JSON.parse(authToken));
					sessionStorage.removeItem('auth-token');
					this.props.setStep1Data(JSON.parse(step1Data));
					sessionStorage.removeItem('step1');
					this.props.setStep2Data(JSON.parse(step2Data));
					sessionStorage.removeItem('step2');
					this.props.setStep3Data(JSON.parse(step3Data));
					sessionStorage.removeItem('step3');
				}
				resolve();
			} catch (e) {
				reject(e);
			}
		});
	}
	async UNSAFE_componentWillMount() {
		await this.resetStateData()
			.then(() => {
				if (this.props.authToken === null) {
					this.props.history.push('/');
				}
			});
	}
	componentDidMount() {
		window.addEventListener('beforeunload', (event) => {
			sessionStorage.setItem('auth-token', JSON.stringify(this.props.authToken));
			sessionStorage.setItem('step1', JSON.stringify(this.props.step1Data));
			sessionStorage.setItem('step2', JSON.stringify(this.props.step2Data));
			sessionStorage.setItem('step3', JSON.stringify(this.props.step3Data));
		});
	}
	render() {
		return (
			<div className='App containerBody'>
				<Switch>
					<Route exact path='/' component={props => <Login {...props} />} />
					<Route exact path='/dashboard' component={props => <Dashboard {...props} />} />
					<Route exact path='/dashboard/newblog' component={props => <Dashboard {...props} />} />
					<Route exact={true} path='*' component={() => <NotFound />} />
				</Switch>
			</div >
		);
	}
}
const mapStateToPros = state => {
	return {
		authToken: state.auth.authToken,
		step1Data: state.blogData.step1Data,
		step2Data: state.blogData.step2Data,
		step3Data: state.blogData.step3Data,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setAauthToken: (token) => dispatch(authTokenAccess(token)),
		setStep1Data: (data) => dispatch(blogStep1Data(data)),
		setStep2Data: (data) => dispatch(blogStep2Data(data)),
		setStep3Data: (data) => dispatch(blogStep3Data(data)),
	}
}

export default withRouter(connect(mapStateToPros, mapDispatchToProps)(App));