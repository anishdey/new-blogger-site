import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createAppStore } from './store/createStore';

const app = (
	<Provider store={createAppStore}>
		<HashRouter>
			<App />
		</HashRouter>
	</Provider>
);

ReactDOM.render(app, document.getElementById('blogger-pallet'));
serviceWorker.unregister();
