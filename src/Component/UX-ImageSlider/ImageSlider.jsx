import React, { Component } from 'react';
import { Carousel } from 'react-bootstrap';
import './ImageSlider.scss';

class ImageSlider extends Component {
    state = {
    }
    render() {
        const { imageArray } = this.props;
        return (
            <Carousel>
                {
                    imageArray && imageArray.length > 0 ?
                        imageArray.map((imageUrl, k) => (
                            <Carousel.Item key={k}>
                                <img
                                    className='d-block w-100 carouselImage'
                                    src={imageUrl}
                                    alt='Img'
                                />
                            </Carousel.Item>
                        )) : null
                }
            </Carousel>
        );
    }
}

export default ImageSlider;
