import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { TiTimes } from "react-icons/ti";
import data from '../../Configs/data.json';
import './LeftMenu.scss';
const configStrings = data;

const LeftMenu = (props) => {
    return (
        <Fragment>
            <div className='titlePannel'>
                <span className='titleText'>Bloger Palette</span>
                <span className='closeBtn' onClick={() => props.leftMenuClose(false)}><TiTimes /></span>
            </div>
            <ul className='customUL'>
                <li>
                    <NavLink className='navLink' to={configStrings.pageUrl.Dashboard}>
                        {configStrings.leftMenuLink.Dashboard}
                    </NavLink>
                </li>
                <li>
                    <NavLink className='navLink' to={configStrings.pageUrl.NewBlog}>
                        {configStrings.leftMenuLink.NewBlog}
                    </NavLink>
                </li>
                <li>
                    <NavLink className='navLink' to={configStrings.pageUrl.Dashboard}>
                        {configStrings.leftMenuLink.ManageBlog}
                    </NavLink>
                </li>
                <li>
                    <NavLink className='navLink' to={configStrings.pageUrl.Dashboard}>
                        {configStrings.leftMenuLink.Drafts}
                    </NavLink>
                </li>
                <li>
                    <NavLink className='navLink' to={configStrings.pageUrl.Dashboard}>
                        {configStrings.leftMenuLink.Profile}
                    </NavLink>
                </li>
            </ul>
        </Fragment>
    );
}

export default LeftMenu;