import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

class ConfirmationModal extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            show: false,
            modalMsg: '',
            modalHeading: ''
        };
    }

    handleClose = (userConfirm) => {
        this.setState({
            show: false,
            modalMsg: '',
            modalHeading: ''
        });
        userConfirm ? this.props.onUserConfirmed() : this.props.onClose();
    }
    static getDerivedStateFromProps(props, state) {
        if (props.isOpen && props.modalMsg !== state.modalMsg) {
            return {
                ...state,
                show: props.isOpen,
                modalMsg: props.modalMsg,
                modalHeading: props.modalHeading
            }
        }
        return null;
    }

    render() {
        const { modalMsg, show, modalHeading } = this.state;
        return (
            <Modal show={show} onHide={this.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{modalHeading}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5>{modalMsg}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="success" onClick={() => this.handleClose(false)}>No</Button>
                    <Button bsStyle="warning" onClick={() => this.handleClose(true)}>Yes</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default ConfirmationModal;
