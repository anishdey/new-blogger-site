import React from 'react';
import { Row, Col } from 'react-bootstrap';
import './FooterComponent.scss';

const FooterComponent = (props) => {
    return (
        <div className='bottomSection'>
            <span>&copy;&nbsp;{new Date().getFullYear()}&nbsp;Copyright: BloggerPallet.com</span>
        </div>
    );
}

export default FooterComponent;
