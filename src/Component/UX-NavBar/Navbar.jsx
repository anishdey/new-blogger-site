import React, { Component } from 'react';
import { Navbar, Nav, Badge } from 'react-bootstrap';
import { TiThMenu, TiBell, TiThList } from "react-icons/ti";
import { RiMailUnreadLine } from "react-icons/ri";
import profileImg from '../../Assets/Images/profileImg.jpg';
import CustomPopOver from '../UX-CustomPopOver/CustomPopover';
import data from '../../Configs/data.json';
import './Navbar.scss';
const configStrings = data;
class NavBar extends Component {
    state = {
        showPopOver: false,
        popOverTarget: null,
        prevId: '',
        popOverId: '',
        popOveTitle: '',
        popoverContent: []
    }
    handleTogglePopover = (e, idString) => {
        let popOverId = '', popOveTitle = '', popoverContent = [];
        let prevId = '';
        switch (idString) {
            case 'newNotification':
                popOverId = idString;
                popOveTitle = 'New Notifications';
                popoverContent = ['Anish Dey Likes you blog.', 'Sampa Sarkar commented on your blog.'];
                break;
            case 'newEmail':
                popOverId = idString;
                popOveTitle = 'New Email';
                popoverContent = ['You have an unread mail form Anish Dey.', 'You have an unread mail form Sampa Sarkar.']
                break;
            case 'toDoList':
                popOverId = idString;
                popOveTitle = 'To Do List';
                popoverContent = ['Create new blog chain for yoga.', 'Instagram post management.'];
                break;
        }
        this.setState({
            prevId: popOverId,
            popOverId,
            popOveTitle,
            popoverContent,
            showPopOver: this.state.prevId !== idString ? true : !this.state.showPopOver,
            popOverTarget: e.target
        })
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handelLogOut = () => {
        this.props.setAauthToken(null);
        this.props.history.push('/');
    }
    render() {
        const {
            popOverId,
            popOveTitle,
            showPopOver,
            popOverTarget,
            popoverContent
        } = this.state;
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand onClick={() => this.props.leftMenuOpen(true)} >
                    <TiThMenu />
                </Navbar.Brand>
                <Navbar.Brand className='mr-auto' href="#dashboard">
                    Blogger Palette
                </Navbar.Brand>
                <Nav className='ml-auto hide-in-sm-device'>
                    <Nav.Link href="#features">Features</Nav.Link>
                    <Nav.Link href="#pricing">Pricing</Nav.Link>
                </Nav>
                <Nav className='icoSetSection'>
                    <Nav.Link className='icoItems'
                        onClick={(e) => this.handleTogglePopover(e, 'newEmail')}
                    >
                        <RiMailUnreadLine />
                        <Badge className='notificationBadge' pill variant="info">2</Badge>
                    </Nav.Link>
                    <Nav.Link className='icoItems'
                        onClick={(e) => this.handleTogglePopover(e, 'toDoList')}
                    >
                        <TiThList />
                    </Nav.Link>
                    <Nav.Link className='icoItems'
                        onClick={(e) => this.handleTogglePopover(e, 'newNotification')}
                    >
                        <TiBell />
                        <Badge className='notificationBadge' pill variant="primary">2</Badge>
                    </Nav.Link>
                </Nav>
                <Nav className='userNameSection'>
                    <Nav.Link id='userImg'>
                        <img src={profileImg} className='profilePic'></img>
                    </Nav.Link>
                    <Nav.Link id='userName'>
                        Jeneva Sarkar
                    </Nav.Link>
                </Nav>
                <CustomPopOver
                    content={popoverContent}
                    id={popOverId}
                    title={popOveTitle}
                    show={showPopOver}
                    target={popOverTarget}
                />
            </Navbar>
        );
    }
}
export default NavBar;
