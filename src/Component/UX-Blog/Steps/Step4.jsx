import React, { Component, Fragment } from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import draftToHtml from 'draftjs-to-html';
import ImageSlider from '../../UX-ImageSlider/ImageSlider';
import data from '../../../Configs/data.json';
import BlogDemoImg1 from '../../../Assets/Images/BlogDemoImg1.jpg';
import BlogDemoImg2 from '../../../Assets/Images/BlogDemoImg2.jpg';
import BlogDemoImg3 from '../../../Assets/Images/BlogDemoImg3.jpg';
const configStrings = data;

class blogStep4 extends Component {
    state = {
        htmlData: null,
        step3DataArray: [],
        imageArray1: [BlogDemoImg1, BlogDemoImg2, BlogDemoImg3]
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handleGetBlogData = () => {
        let step3DataArray;
        if (this.props.step3Data) {
            step3DataArray = Object.values(this.props.step3Data);
            this.setState({
                step3DataArray
            })
        }
        console.log(step3DataArray);
    }
    componentDidMount() {
        this.handleGetBlogData();
    }
    render() {
        const { step3DataArray } = this.state;
        return (
            <div className='container readBlogsContainer'>
                <Row id='mainContent'>
                    <Col md={12} sm={12} xs={12}>
                        <div className='youtubeLinkBlock'>
                            <iframe
                                width='100%'
                                height='315'
                                src='https://www.youtube.com/embed/coB1HpY8oGY'
                                frameBorder='0'
                                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen='allowfullscreen'
                                mozallowfullscreen='mozallowfullscreen'
                                msallowfullscreen='msallowfullscreen'
                                oallowfullscreen='oallowfullscreen'
                                webkitallowfullscreen='webkitallowfullscreen'
                            ></iframe>
                        </div>
                        <div className='descriptionBlock'>
                            <h1 className='blogHeadingText'>
                                {this.props.step1Data && this.props.step1Data.title}
                            </h1>
                            <p className='blogSubHeadingText'>
                                {this.props.step1Data && this.props.step1Data.subTitle} · By Jeneva Sarkar · July 16,2020 · 453 views
                            </p>
                            <h5 className='blogDescriptionText'>
                                {this.props.step2Data && this.props.step2Data.description}
                            </h5>
                            <hr />
                        </div>
                        {
                            step3DataArray.length > 0 ?
                                step3DataArray.map((htmlData, k) => (
                                    <div
                                        key={k}
                                        className='sectionBlock'
                                    >
                                        <ImageSlider imageArray={this.state.imageArray1} />
                                        <div
                                            className='sectionContent-text'
                                            dangerouslySetInnerHTML={{ __html: draftToHtml(htmlData.rawData) }}
                                        >
                                        </div>
                                    </div>
                                ))
                                : null
                        }
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        step1Data: state.blogData.step1Data,
        step2Data: state.blogData.step2Data,
        step3Data: state.blogData.step3Data
    }
}
export default connect(mapStateToProps)(blogStep4);
