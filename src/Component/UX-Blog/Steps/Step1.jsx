import React, { Component } from 'react';
import { Row, Col, FormControl } from 'react-bootstrap';
import ButtonSet from '../../UX-ButtonSet/ButtonSet';
import { Field, Form, Formik } from 'formik';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { blogStep1Data } from '../../../store/actions/blogData';
import data from '../../../Configs/data.json';
const configStrings = data;

const step1initialValues = {
    title: '',
    subTitle: '',
}
//Formik and Yup validation
const step1ValidationSchema = Yup.object().shape({
    title: Yup.string()
        .required("Title can't be blank"),

    subTitle: Yup.string()
        .required("Sub-title can't be blank")
});
class BlogStep1 extends Component {
    state = {
        step1Value: null
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handleSubmit = (values) => {
        this.setState({ step1Value: values }, () => console.log('step1', this.state.step1Value));
        this.props.setStep1Data(values);
        this.handelRedirect(configStrings.pageUrl.NewBlogStep2);
    };
    render() {
        const { step1Data } = this.props;
        if (!this.props.isNewBlog && step1Data) {
            Object.assign(step1initialValues, step1Data);
        }
        return (
            <div className='container'>
                <Formik
                    initialValues={step1initialValues}
                    validationSchema={step1ValidationSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ values, errors, touched, handleBlur, handleChange, setFieldValue }) => {
                        return (
                            <Form className='formStyle'>
                                <Row>
                                    <Col md={12} xs={12} className='noPadding'>
                                        <label>{configStrings.NewBlogStep1Componenet.TitleText}</label>
                                        <Field
                                            name='title'
                                            type='text'
                                            className={`form-control`}
                                            autoComplete='nope'
                                            placeholder={configStrings.NewBlogStep1Componenet.TitlePlaceholder}
                                        />
                                        {errors.title && touched.title ? (
                                            <span className='formErrorMsg'>{errors.title}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} xs={12} className='noPadding'>
                                        <label>{configStrings.NewBlogStep1Componenet.SubTitleText}</label>
                                        <Field
                                            name='subTitle'
                                            type='text'
                                            className={`form-control`}
                                            autoComplete='nope'
                                            placeholder={configStrings.NewBlogStep1Componenet.SubTitlePlaceholder}
                                        />
                                        {errors.subTitle && touched.subTitle ? (
                                            <span className='formErrorMsg'>{errors.subTitle}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </Col>
                                </Row>
                                <Row>
                                    <ButtonSet
                                        key='Step1'
                                        hideAddSection={true}
                                        handleBack={() => this.handelRedirect(configStrings.pageUrl.Dashboard)}
                                        isSubmit={true}
                                        {...this.props}
                                    />
                                </Row>
                            </Form>
                        );
                    }}
                </Formik>
            </div>
        );
    }
}
const mapStateToPros = state => {
    return {
        step1Data: state.blogData.step1Data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setStep1Data: (data) => dispatch(blogStep1Data(data)),
    }
}
export default connect(mapStateToPros, mapDispatchToProps)(BlogStep1);
