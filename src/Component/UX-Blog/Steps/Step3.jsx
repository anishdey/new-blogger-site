import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { MdExpandLess, MdRemoveCircleOutline, MdExpandMore } from 'react-icons/md';
import { connect } from 'react-redux';
import ConfirmationModal from '../../UX-ConfirmationModal/confirmationModal';
import ButtonSet from '../../UX-ButtonSet/ButtonSet';
import BlogEditor from '../../UX-Editor/editor';
import data from '../../../Configs/data.json';
import { blogStep3Data } from '../../../store/actions/blogData';
const configStrings = data;

class BlogStep3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sectionArray: [],
            step3DataArray: [],
            step3Value: {},
            hideArray: [],
            showUserConfirmation: false,
            modalMsg: '',
            modalHeading: '',
            selectedSection: ''
        }
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handleSubmit = () => {
        this.handelRedirect(configStrings.pageUrl.NewBlogStep4);
    };
    handleUserConfirmationModal = (visibility, section) => {
        this.setState({
            showUserConfirmation: visibility,
            modalMsg: visibility ? configStrings.actionMsg.dataLost : '',
            modalHeading: visibility ? configStrings.actionMsg.deleteConfirmation : '',
            selectedSection: visibility ? section : ''
        })
    }
    handleAddSection = () => {
        let tempEditorData = {};
        let tempArray = this.state.sectionArray;
        let lastSection = tempArray[tempArray.length - 1];
        let next = +lastSection.slice(-1) + 1;
        let nextSection = 'Section ' + next;
        tempArray.push(nextSection);
        this.setState({ sectionArray: tempArray });
        // if (this.props.editorData) {
        //     tempEditorData[lastSection] = this.props.editorData.blocks[0];
        // }
    }
    handleRemoveSection = () => {
        let tempArray = this.state.sectionArray;
        let section = this.state.selectedSection;
        if (tempArray.length > 1) {
            tempArray.splice(tempArray.indexOf(section), 1);
            this.setState({
                sectionArray: tempArray,
                selectedSection: ''
            });
        }
        this.handleUserConfirmationModal(false);
    }
    handleEditorData = () => {
        let currentSectionData = this.props.editorData;  //taking from redux
        let step3Data = this.props.step3Data ? this.props.step3Data : {};            //taking from redux
        if (currentSectionData) {
            if (currentSectionData.id in step3Data) {
                step3Data[currentSectionData.id].rawData = currentSectionData;
            } else {
                Object.assign(step3Data, { [currentSectionData.id]: {} });
                step3Data[currentSectionData.id].rawData = currentSectionData;
            }
        }
        this.props.setStep3Data(step3Data);
        console.log('step3Data', step3Data);
    }
    handleGetImage = (e, sectionId) => {
        let step3Data = this.state.step3Value;
        if (e.target.files) {
            if (sectionId in step3Data) {
                step3Data[sectionId].imageData = e.target.files;
            } else {
                Object.assign(step3Data, { [sectionId]: {} });
                step3Data[sectionId].imageData = e.target.files;
            }
        }
        this.setState({ step3Value: step3Data });
        console.log('step3Data', step3Data);
    }
    handleShowHide = (section) => {
        let tempArray = this.state.hideArray;
        if (tempArray.indexOf(section) > -1) {
            tempArray.splice(tempArray.indexOf(section), 1);
        } else {
            tempArray.push(section);
        }
        this.setState({ hideArray: tempArray });
    }
    handleGetStep3Data = () => {
        let step3DataArray, tempSectionArray = [];
        if (this.props.step3Data && Object.keys(this.props.step3Data).length !== 0) {
            step3DataArray = Object.values(this.props.step3Data);
            step3DataArray.length > 0 && step3DataArray.map(data => (
                tempSectionArray.push(data.rawData.id)
            ))
            this.setState({
                step3DataArray,
                sectionArray: tempSectionArray
            })
        } else {
            this.setState({
                sectionArray: ['Section 1']
            })
        }
        console.log('step3DataArray', step3DataArray);
    }
    componentDidMount() {
        this.handleGetStep3Data();
    }
    render() {
        const {
            step3DataArray,
            sectionArray,
            hideArray,
            showUserConfirmation,
            modalMsg,
            modalHeading
        } = this.state;
        return (
            <div className='container'>
                {
                    sectionArray.map((section) => (
                        <div className='sectionBlock' key={section}>
                            <Row className='sectionHead'>
                                <div className='sectionTitle'>
                                    {section}
                                </div>
                                <div className='offSet6 sectionController'>
                                    {sectionArray.length > 1 ?
                                        <i onClick={() => this.handleUserConfirmationModal(true, section)}><MdRemoveCircleOutline /></i>
                                        : null
                                    }
                                    <i onClick={() => this.handleShowHide(section)}>
                                        {
                                            hideArray.indexOf(section) > -1 ? <MdExpandMore />
                                                : <MdExpandLess />
                                        }
                                    </i>
                                </div>
                            </Row>
                            <div className={hideArray.indexOf(section) > -1 ? 'hideBlock' : 'showBlock'} >
                                <Row>
                                    <Col md={12} sm={12} className='noPadding'>
                                        <BlogEditor
                                            id={section.trim()}
                                            onEditorValueChange={this.handleEditorData}
                                            {...this.props}
                                        />
                                    </Col>
                                </Row>
                                <br />
                                <Row>
                                    <Col md={12} sm={12} className='noPadding'>
                                        <input
                                            type='file'
                                            style={{ padding: '3px' }}
                                            onChange={(e) => this.handleGetImage(e, section.trim())}
                                            className='form-control'
                                            accept='image/x-png,image/gif,image/jpeg'
                                            multiple
                                        ></input>
                                    </Col>
                                </Row>
                            </div>
                            <br />
                        </div>
                    ))
                }
                <Row className='buttonSetBlock'>
                    <ButtonSet
                        key='Step3'
                        handleAddSection={this.handleAddSection}
                        handleNext={this.handleSubmit}
                        handleBack={() => this.handelRedirect(configStrings.pageUrl.NewBlogStep2)}
                        isSubmit={false}
                        {...this.props}
                    />
                </Row>

                <ConfirmationModal
                    isOpen={showUserConfirmation}
                    modalMsg={modalMsg}
                    modalHeading={modalHeading}
                    onClose={() => this.handleUserConfirmationModal(false)}
                    onUserConfirmed={this.handleRemoveSection}
                />
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        step3Data: state.blogData.step3Data,
        editorData: state.blogData.editorData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setStep3Data: (data) => dispatch(blogStep3Data(data)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(BlogStep3);