import React, { Component } from 'react';
import { Row, Col, FormControl } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import ButtonSet from '../../UX-ButtonSet/ButtonSet';
import { blogStep2Data } from '../../../store/actions/blogData';
import data from '../../../Configs/data.json';
import { MdTrendingUp } from 'react-icons/md';
const configStrings = data;

const step2initialValues = {
    category: '',
    description: ''
}
//Formik and Yup validation
const step2ValidationSchema = Yup.object().shape({
    description: Yup.string()
        .required("description can't be blank"),

    category: Yup.string()
        .required("Please select a category")
});
class BlogStep2 extends Component {
    state = {
        step2Value: null
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handleSubmit = (values) => {
        this.setState({ step2Value: values }, () => console.log('step2', this.state.step2Value));
        this.props.setStep2Data(values);
        this.handelRedirect(configStrings.pageUrl.NewBlogStep3);
    };
    render() {
        const { step2Data } = this.props;
        if (!this.props.isNewBlog && step2Data) {
            Object.assign(step2initialValues, step2Data);
        }
        return (
            <div className='container'>
                <Formik
                    initialValues={step2initialValues}
                    validationSchema={step2ValidationSchema}
                    onSubmit={this.handleSubmit}
                >
                    {({ values, errors, touched, handleBlur, handleChange, setFieldValue }) => {
                        return (
                            <Form className='formStyle'>
                                <Row>
                                    <Col md={12} xs={12} className='noPadding'>
                                        <label>Select category</label>
                                        <Field
                                            name='category'
                                            component='select'
                                            className={`form-control`}
                                        >
                                            <option value=''>-- Select --</option>
                                            <option value='lifestyle'>Life Style</option>
                                            <option value='health'>Health and Diet</option>
                                            <option value='skincare'>Skin Care</option>
                                            <option value='haircare'>Hair Care</option>
                                        </Field>
                                        {errors.category && touched.category ? (
                                            <span className='formErrorMsg'>{errors.category}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} xs={12} className='noPadding'>
                                        <label>description</label>
                                        <Field
                                            name='description'
                                            type='text'
                                            className={`form-control`}
                                            autoComplete='nope'
                                            placeholder='description'
                                        />
                                        {errors.description && touched.description ? (
                                            <span className='formErrorMsg'>{errors.description}</span>
                                        ) : null}
                                        <FormControl.Feedback />
                                    </Col>
                                </Row>
                                <Row>
                                    <ButtonSet
                                        key='Step2'
                                        hideAddSection={true}
                                        handleNext={this.handleSubmit}
                                        handleBack={() => this.handelRedirect(configStrings.pageUrl.NewBlogStep1)}
                                        isSubmit={true}
                                        {...this.props}
                                    />
                                </Row>
                            </Form>
                        );
                    }}
                </Formik>

            </div>
        );
    }
}
const mapStateToPros = state => {
    return {
        step2Data: state.blogData.step2Data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setStep2Data: (data) => dispatch(blogStep2Data(data)),
    }
}
export default connect(mapStateToPros, mapDispatchToProps)(BlogStep2);

