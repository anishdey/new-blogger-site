import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Step1 from './Steps/Step1';
import Step2 from './Steps/Step2';
import Step3 from './Steps/Step3';
import Step4 from './Steps/Step4';
import ProgressBar from '../UX-CustomProgressBar/CustomProgressBar';
import './NewBlog.scss';
import data from '../../Configs/data.json';
const configStrings = data;

class NewBlog extends Component {
    state = {
        totalSteps: [0, 25, 50, 75, 100],
        completion: 0,
        stepNo: '',
        isNewBlog: false
    }
    calculateSteps = () => {
        var completion = 0;
        var blogStep = this.props.location.search;
        var stepNo = blogStep.substring(blogStep.length - 1, blogStep.length);
        if (stepNo === '1') {
            completion = 0;
        } else if (stepNo === '2') {
            completion = 25;
        } else if (stepNo === '3') {
            completion = 50;
        } else if (stepNo === '4') {
            completion = 75;
        } else {
            completion = 100;
        }
        this.setState({
            stepNo,
            isNewBlog: this.props.location.pathname.search("newBlog") > -1 ? true : false,
            completion
        })
    }
    UNSAFE_componentWillMount() {
        this.calculateSteps();
    }
    render() {
        const { completion, stepNo, totalSteps, isNewBlog } = this.state;
        return (
            <div className='container'>
                <Row className='headingController'>
                    <Col md={12} sm={12}>
                        {
                            stepNo === '1' ? <span className='stepInfo'>{configStrings.NewBlogComponenet.Title1}</span>
                                : stepNo === '2' ? <span className='stepInfo'>{configStrings.NewBlogComponenet.Title2}</span>
                                    : stepNo === '3' ? <span className='stepInfo'>{configStrings.NewBlogComponenet.Title3}</span>
                                        : <span className='stepInfo'>{configStrings.NewBlogComponenet.Title4}</span>
                        }
                    </Col>
                </Row>
                <Row className='progressController'>
                    <Col md={12} sm={12}>
                        <ProgressBar completion={completion} totalSteps={totalSteps} />
                    </Col>
                </Row>
                <Row className='stepController'>
                    <Col md={12} sm={12}>
                        {
                            stepNo === '1' ? <Step1 isNewBlog={isNewBlog} key={'step1Component'} {...this.props} />
                                : stepNo === '2' ? <Step2 isNewBlog={isNewBlog} key={'step2Component'} {...this.props} />
                                    : stepNo === '3' ? <Step3 isNewBlog={isNewBlog} key={'step3Component'} {...this.props} />
                                        : stepNo === '4' ? <Step4 key={'step4Component'} {...this.props} />
                                            : null
                        }
                    </Col>
                </Row>
            </div>
        );
    }
}
export default NewBlog;
