import React, { Component } from 'react';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import '../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { editorData } from '../../store/actions/blogData';
//import "../../../node_modules/draft-js-image-plugin/lib/plugin.css";

class BlogEditor extends Component {
    state = {
        editorState: EditorState.createEmpty(),
        convertedData: null,
        EditorData: []
    }

    onEditorStateChange = (editorState) => {
        let convertedData = convertToRaw(editorState.getCurrentContent());
        this.setState({
            editorState
        });
        convertedData.id = this.props.id;
        this.props.returnEditorData(convertedData);
    };

    render() {
        const { editorState } = this.state;
        return (
            <div className='blogEditor'>
                <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    onEditorStateChange={this.onEditorStateChange}
                />
                <textarea
                    className='hide'
                    disabled
                    value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                />
            </div>
        );
    }
}
export default BlogEditor;