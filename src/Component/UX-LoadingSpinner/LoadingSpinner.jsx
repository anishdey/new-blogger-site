import React from 'react';
import Loader from 'react-loader-spinner';
import './LoadingSpinner.scss'

const LoadingSpinner = () => (
	<div className='container loadingSpinnerContainer'>
		<div className="loadingSpinner">
			<Loader
				type="ThreeDots"
				color="White"
				height="100"
				width="100"
			/>
		</div>
	</div>
);

export default LoadingSpinner;