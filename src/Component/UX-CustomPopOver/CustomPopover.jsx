import React from 'react';
import { Overlay, Popover } from 'react-bootstrap';
import data from '../../Configs/data.json';
import './CustomePopOver.scss';
const configStrings = data;

const CustomPopOver = (props) => {
    return (
        <Overlay
            show={props.show}
            target={props.target}
            placement="bottom"
            containerPadding={20}
        >
            <Popover id={props.id && props.id}>
                <Popover.Title as="h3">{props.title && props.title}</Popover.Title>
                <Popover.Content>
                    {
                        props.content && props.content.length > 0 ?
                            props.content.map((content, k) => (
                                <p className='notificationItem' key={k}>{content}</p>
                            )) :
                            <p key={1}>
                                {configStrings.noItemText}
                            </p>
                    }
                </Popover.Content>
                <p className='popoverFooter'>
                    <strong>Show All</strong>
                </p>
            </Popover>
        </Overlay>
    );
}
export default CustomPopOver;