import Loader from 'react-loader-spinner';
import React from 'react';
import './loaderButton.scss';

const LoaderButton = () => (
    <div className="loaderButton">
        <Loader
            type="Oval"
            color='#fff'
            height={15}
            width={15}
        />
    </div>
);

export default LoaderButton;