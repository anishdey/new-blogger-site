import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState, convertFromHTML } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import { connect } from 'react-redux';
import '../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { editorData } from '../../store/actions/blogData';
import './editor.scss';

class BlogEditor extends Component {
    state = {
        editorState: EditorState.createEmpty(),
        convertedData: null
    }

    onEditorStateChange = (editorState) => {
        let convertedData = convertToRaw(editorState.getCurrentContent());
        this.setState({
            editorState
        });
        convertedData.id = this.props.id;
        this.props.setEditorData(convertedData);  //save editor data to redux
        this.props.onEditorValueChange();
    };
    componentDidMount() {
        if (this.props.step3Data && this.props.step3Data[this.props.id]) {
            //setting with default value if draft;
            this.setState({
                editorState: EditorState.createWithContent(
                    ContentState.createFromBlockArray(
                        convertFromHTML(draftToHtml(this.props.step3Data[this.props.id].rawData))
                    )
                )
            })
        }
    }

    render() {
        const { editorState } = this.state;
        return (
            <div className='blogEditor'>
                <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    onEditorStateChange={this.onEditorStateChange}
                />
                {/* <textarea
                    className='hide'
                    disabled
                    value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                /> */}

            </div>
        );
    }
}

const mapStateToPros = state => {
    return {
        editorData: state.blogData.editorData,
        step3Data: state.blogData.step3Data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setEditorData: (data) => dispatch(editorData(data)),
    }
}
export default connect(mapStateToPros, mapDispatchToProps)(BlogEditor);