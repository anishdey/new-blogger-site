import React, { Component } from 'react';
import { AiFillCheckCircle } from "react-icons/ai";
import { IconContext } from "react-icons";
import 'react-step-progress-bar/styles.css';
import { ProgressBar, Step } from 'react-step-progress-bar';
import './CustomProgressBar.scss';

class customProgressBar extends Component {
    state = {
        completion: 0,
        totalSteps: []
    }
    componentDidMount() {
        this.setState({
            completion: this.props.completion,
            totalSteps: this.props.totalSteps
        });
    }
    render() {
        const { completion, totalSteps } = this.state;
        return (
            <ProgressBar
                percent={completion}
                stepPositions={totalSteps}
                filledBackground='linear-gradient(90deg, rgba(118,25,167,1) 0%, rgba(215,110,218,1) 47%, rgba(255,127,127,1) 100%)'
            >
                {totalSteps && totalSteps.length > 0 ?
                    totalSteps.map((step, k) => (
                        <Step key={k} transition="scale">
                            {({ accomplished }) => (
                                <IconContext.Provider
                                    value={{ className: `reac-icons steps ${accomplished ? 'completed' : 'inComplete'}` }}>
                                    <div>
                                        <AiFillCheckCircle />
                                    </div>
                                </IconContext.Provider>
                            )}
                        </Step>
                    ))
                    : null
                }
            </ProgressBar>
        );
    }
}
export default customProgressBar;
