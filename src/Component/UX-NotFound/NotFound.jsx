import React, { Component } from 'react';
import data from '../../Configs/data.json';
import { Row, Col } from 'react-bootstrap';
const configStrings = data;

class NotFound extends Component {
    render() {
        return (
            <div className='container errorFont'>
                <Row>
                    <Col md={12} sm={12}>
                        <h1>{configStrings.actionMsg.pageNotFoundText}</h1>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default NotFound;