import React, { Component } from 'react';
import { connect } from 'react-redux';
import { resetBlogData } from '../../store/actions/blogData';
import ConfirmationModal from '../UX-ConfirmationModal/confirmationModal';
import data from '../../Configs/data.json';
import './ButtonSet.scss';
const configStrings = data;

class ButtonSet extends Component {
    state = {
        step3Value: null,
        modalMsg: '',
        modalHeading: '',
        showUserConfirmation: false
    }

    handleConfirmationModal = (visibility) => {
        this.setState({
            showUserConfirmation: visibility,
            modalMsg: visibility ? configStrings.actionMsg.blogDataLost : '',
            modalHeading: visibility ? configStrings.actionMsg.createDraft : ''
        });
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handleCreateDraft = () => {
        this.props.handleResetBlogData(null);
        this.handleConfirmationModal(false);
    }
    handleDiscardBlog = () => {
        this.props.handleResetBlogData(null);
        this.handleConfirmationModal(false);
        this.handelRedirect(configStrings.pageUrl.Dashboard);
    }
    render() {
        const { hideAddSection, isSubmit } = this.props;
        const { showUserConfirmation, modalMsg, modalHeading } = this.state;
        return (
            <div className='buttonSet'>
                <button
                    className='customButton-Blog leftMostBtn'
                    onClick={this.props.handleBack}
                >
                    <span>Back</span>
                </button>
                <button
                    type='button'
                    className='customButton-Blog'
                    onClick={() => this.handleConfirmationModal(true)}
                >
                    <span>Cancle</span>
                </button>
                {
                    !hideAddSection ?
                        <button
                            className='customButton-Blog offSetBtn'
                            onClick={this.props.handleAddSection}
                        >
                            <span className='addSection'>Add Section</span>
                        </button>
                        : null
                }
                {isSubmit ?
                    <button
                        type='submit'
                        className={'customButton-Blog rightMostBtn ' + (hideAddSection ? 'offSetLgBtn' : null)}
                    >
                        <span>Next</span>
                    </button>
                    :
                    <button
                        onClick={this.props.handleNext}
                        className={'customButton-Blog rightMostBtn ' + (hideAddSection ? 'offSetLgBtn' : null)}
                    >
                        <span>Next</span>
                    </button>
                }
                <ConfirmationModal
                    isOpen={showUserConfirmation}
                    modalMsg={modalMsg}
                    modalHeading={modalHeading}
                    onClose={() => this.handleDiscardBlog()}
                    onUserConfirmed={() => this.handleCreateDraft()}
                />
            </div>
        );
    }
}

const mapStateToPros = state => {
    return {
        step1Data: state.blogData.step1Data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        handleResetBlogData: (data) => dispatch(resetBlogData(data))
    }
}
export default connect(mapStateToPros, mapDispatchToProps)(ButtonSet);