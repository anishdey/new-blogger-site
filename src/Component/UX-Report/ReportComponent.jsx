import React, { Component } from 'react';
import { Row, Col, Badge, Table } from 'react-bootstrap';
import { Doughnut, Bar } from 'react-chartjs-2';
import {
    AiOutlineUsergroupAdd,
    AiOutlineLineChart,
    AiFillDelete,
    AiFillEdit
} from "react-icons/ai";
import data from '../../Configs/data.json';
import demoChartData from './demoChartData.json';
import './ReportComponent.scss';
const configStrings = data;
const chartData = demoChartData;
class ReportComponent extends Component {
    state = {
        isLeftMenuOpen: false
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    render() {
        const { isLeftMenuOpen } = this.state;
        return (
            <div className='container ReportComponent'>
                <Row>
                    <Col md={3} sm={3} xs={6}>
                        <div className='card-sm primary-gradient'>
                            <div className='card-sm-content'>
                                <p>New Visits</p>
                                <p>
                                    <Badge pill variant="info">
                                        <AiOutlineUsergroupAdd /> 30
                                    </Badge>
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col md={3} sm={3} xs={6}>
                        <div className='card-sm info-gradient'>
                            <div className='card-sm-content'>
                                <p>New Subscribers</p>
                                <p>
                                    <Badge pill variant="primary">
                                        <AiOutlineUsergroupAdd /> 5
                                    </Badge>
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col md={3} sm={3} xs={6} className='responsiveCard-sm'>
                        <div className='card-sm warning-gradient'>
                            <div className='card-sm-content'>
                                <p>Traffic Growth</p>
                                <p>
                                    <Badge pill variant="warning">
                                        <AiOutlineLineChart /> 12%
                                    </Badge>
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col md={3} sm={3} xs={6} className='responsiveCard-sm'>
                        <div className='card-sm danger-gradient'>
                            <div className='card-sm-content'>
                                <p>Activity</p>
                                <p>
                                    <Badge pill variant="danger">
                                        <AiOutlineLineChart /> 22%
                                    </Badge>
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col md={6} sm={6} xs={12}>
                        <div className='card-md'>
                            <div className='chartContent'>
                                <h3>Traffic</h3>
                                <span>August 16, 2020</span>
                                <Bar
                                    data={chartData.BarChartData}
                                    options={{
                                        maintainAspectRatio: false,
                                        scales: {
                                            xAxes: [{
                                                gridLines: {
                                                    drawOnChartArea: false
                                                }
                                            }],
                                            yAxes: [{
                                                gridLines: {
                                                    drawOnChartArea: false
                                                }
                                            }],
                                        }
                                    }}
                                />
                            </div>
                        </div>
                    </Col>
                    <Col md={6} sm={6} xs={12} className='responsiveCard-sm'>
                        <div className='card-md'>
                            <div className='chartContent'>
                                <h3>Social Media</h3>
                                <span>August 16, 2020</span>
                                <Doughnut
                                    data={chartData.DoughnutData}
                                    width={100}
                                    height={50}
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col md={12} sm={12} xs={12}>
                        <Table className='blog-content-table' striped bordered hover variant='dark' responsive='sm'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Created on</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>Health and Diet</td>
                                    <td>26/08/2020</td>
                                    <td className='td-Action'>
                                        <i><AiFillDelete /></i>
                                        &nbsp; | &nbsp;
                                        <i><AiFillEdit /></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>Health and Diet</td>
                                    <td>26/08/2020</td>
                                    <td className='td-Action'>
                                        <i><AiFillDelete /></i>
                                        &nbsp; | &nbsp;
                                        <i><AiFillEdit /></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>Health and Diet</td>
                                    <td>26/08/2020</td>
                                    <td className='td-Action'>
                                        <i><AiFillDelete /></i>
                                        &nbsp; | &nbsp;
                                        <i><AiFillEdit /></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>15 Benifits of Yoga</td>
                                    <td>Health and Diet</td>
                                    <td>26/08/2020</td>
                                    <td className='td-Action'>
                                        <i><AiFillDelete /></i>
                                        &nbsp; | &nbsp;
                                        <i><AiFillEdit /></i>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default ReportComponent;
