import {
    BLOG_STEP1_DATA,
    BLOG_STEP2_DATA,
    BLOG_STEP3_DATA,
    BLOG_EDITOR_DATA,
    BLOG_DATA_RESET
} from "./actionTypes";

export const blogStep1Data = (data) => {
    return {
        type: BLOG_STEP1_DATA,
        step1Data: data
    }
}
export const blogStep2Data = (data) => {
    return {
        type: BLOG_STEP2_DATA,
        step2Data: data
    }
}
export const blogStep3Data = (data) => {
    return {
        type: BLOG_STEP3_DATA,
        step3Data: data
    }
}
export const editorData = (data) => {
    return {
        type: BLOG_EDITOR_DATA,
        editorData: data
    }
}
export const resetBlogData = (data) => {
    return {
        type: BLOG_DATA_RESET,
        step1Data: data,
        step2Data: data,
        step3Data: data,
        editorData: data
    }
}