import {
    AUTH_TOKEN
} from "./actionTypes";

export const authTokenAccess = (data) => {
    return {
        type: AUTH_TOKEN,
        token: data
    }
}