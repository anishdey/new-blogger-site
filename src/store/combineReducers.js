import { combineReducers } from 'redux';
// Import reducer files
import Auth from './reducers/auth';
import BlogData from './reducers/blogData';

export const AppCombineReducers = combineReducers({
    auth : Auth,
    blogData: BlogData
});
