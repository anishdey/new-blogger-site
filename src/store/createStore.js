import { createStore } from 'redux';

import { AppCombineReducers } from "./combineReducers";
import { Enhancers } from "./enhancer";

export const createAppStore = createStore(AppCombineReducers, Enhancers);