import {
    BLOG_STEP1_DATA,
    BLOG_STEP2_DATA,
    BLOG_STEP3_DATA,
    BLOG_EDITOR_DATA,
    BLOG_DATA_RESET
} from "../actions";

const initialState = {
    step1Data: null,
    step2Data: null,
    step3Data: null,
    editorData: null
}

const step1Data = (state, action) => {
    return {
        ...state,
        step1Data: action.step1Data
    }
}
const step2Data = (state, action) => {
    return {
        ...state,
        step2Data: action.step2Data
    }
}
const step3Data = (state, action) => {
    return {
        ...state,
        step3Data: action.step3Data
    }
}
const editorData = (state, action) => {
    return {
        ...state,
        editorData: action.editorData
    }
}
const resetBlogData = (state, { step1Data, step2Data, step3Data, editorData }) => {
    return {
        ...state,
        step1Data,
        step2Data,
        step3Data,
        editorData
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case BLOG_STEP1_DATA: return step1Data(state, action);
        case BLOG_STEP2_DATA: return step2Data(state, action);
        case BLOG_STEP3_DATA: return step3Data(state, action);
        case BLOG_EDITOR_DATA: return editorData(state, action);
        case BLOG_DATA_RESET: return resetBlogData(state, action);
        default: return state;
    }
}

export default reducer;