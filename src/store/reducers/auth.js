import {
    AUTH_TOKEN,
} from "../actions";

const initialState = {
    authToken: null
}

const authToken = (state, action) => {
    return {
        ...state,
        authToken: action.token
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_TOKEN: return authToken(state, action);
        default: return state;
    }
}

export default reducer;