import React, { Component } from 'react';
import axios from '../../Axios/eaxios';
import { connect } from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import LoaderButton from '../../Component/UX-LoaderButton/loaderButton';
import { authTokenAccess } from '../../store/actions/auth';
import data from '../../Configs/data.json';
import './Login.scss';
const configStrings = data.loginComponent;

class Login extends Component {
    state = {
        userId: '',
        password: '',
        isLoginLoading: false,
        errorMsgLogin: '',
        loginStatus: '',
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handelField = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handelLogin = (url) => {
        var payload = {
            'userId': this.state.userId,
            'password': this.state.password
        }
        this.setState({ isLoginLoading: true }, () => {
            axios.post('/authentication/login', payload)
                .then(res => {
                    if (res.data.status === 200) {
                        this.setState({
                            loginStatus: 'sucess',
                            isLoginLoading: false
                        }, () => {
                            this.props.setAauthToken('jeneva');
                            this.props.history.push('/dashboard');
                            sessionStorage.setItem('E1QY5ZOTD', this.state.userId);
                        })
                    } else {
                        this.setState({
                            loginStatus: 'failed',
                            errorMsgLogin: res.data && res.data.confirmation,
                            isLoginLoading: false
                        })
                        setTimeout(() => {
                            this.setState({ errorMsgLogin: '', loginStatus: '', });
                        }, 2000);
                    }

                })
                .catch(err => {
                    this.setState({
                        loginStatus: 'failed',
                        errorMsgLogin: err.response && err.response.data.message,
                        isLoginLoading: false
                    })
                    setTimeout(() => {
                        this.setState({ errorMsgLogin: '', loginStatus: '', });
                    }, 2000);
                });
        })
    }
    render() {
        const { isLoginLoading, errorMsgLogin, loginStatus } = this.state;
        return (
            <div className='container'>
                <div className='fieldBlock'>
                    <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className='headingBlock'>
                            <h2>Login&nbsp;
                            </h2>
                        </Col>
                    </Row>
                    <Row className='alertBlock'>
                        <Col lg={12} md={12} sm={12} xs={12}>
                            {
                                loginStatus === 'failed' ?
                                    <p className='errorMsg'>{errorMsgLogin ? errorMsgLogin : configStrings.loginFailedMsg}</p>
                                    : null
                            }
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className=''>
                            {/* <label>{configStrings.loginIdText}</label> */}
                            <input
                                type='text'
                                name={configStrings.loginId}
                                className='customTextBox'
                                placeholder={configStrings.loginIdPlaceholder}
                                onChange={(e) => this.handelField(e)}
                            ></input>
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={12} md={12} sm={12} xs={12} className=''>
                            {/* <label>{configStrings.loginPasswordText}</label> */}
                            <input
                                type='password'
                                name={configStrings.loginPassword}
                                className='customTextBox'
                                placeholder={configStrings.loginPasswordPlaceholder}
                                onChange={(e) => this.handelField(e)}
                            ></input>
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col md={12} className=''>
                            <Button
                                bsStyle='primary'
                                onClick={this.handelLogin}
                                disabled={isLoginLoading}
                                className='customButton'
                            >
                                {isLoginLoading ? <LoaderButton /> : configStrings.loginButtonText}
                            </Button>
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col md={10} className=''>
                            <span className='forgetPassword'>{configStrings.forgetPasswordtext}</span>
                            <br />
                            <span className='forgetPassword' onClick={() => this.handelRedirect('/signup')}>
                                {configStrings.notAUserText}
                            </span>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setAauthToken: (token, id) => dispatch(authTokenAccess(token)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
