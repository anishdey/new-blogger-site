import React, { Component, Fragment } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { authTokenAccess } from '../../store/actions/auth';

import NavBar from '../../Component/UX-NavBar/Navbar';
import LeftMenu from '../../Component/UX-LeftMenu/LeftMenu';
import ReportComponent from '../../Component/UX-Report/ReportComponent';
import FooterComponent from '../../Component/UX-Footer/FooterComponent';
import NewBlog from '../../Component/UX-Blog/NewBlog';
import data from '../../Configs/data.json';
import './Dashboard.scss';
const configStrings = data;

class Dashboard extends Component {
    state = {
        isLeftMenuOpen: false
    }
    handelRedirect = (url) => {
        this.props.history.push(url);
    }
    handelLogOut = () => {
        this.props.setAauthToken(null);
        this.props.history.push('/');
    }
    handleLeftMenuToggle = (isOpen) => {
        this.setState({ isLeftMenuOpen: isOpen });
    }
    render() {
        const { isLeftMenuOpen } = this.state;
        return (
            <Fragment>
                <Row className='dashboardWrapper'>
                    <div className={`leftMenu ${isLeftMenuOpen ? 'leftMenuOpen' : 'leftMenuClose'}`}>
                        <LeftMenu leftMenuClose={(isOpen) => this.handleLeftMenuToggle(isOpen)} />
                    </div>
                    <div className={`rightContent ${isLeftMenuOpen ? 'blurOn' : 'blurOff'}`}>
                        <Row>
                            <Col lg={12} md={12} sm={12}>
                                <NavBar
                                    leftMenuOpen={(isOpen) => this.handleLeftMenuToggle(isOpen)}
                                />
                            </Col>
                        </Row>
                        <Row className='container-fulid'>
                            <Col md={12} sm={12}>
                                <Switch>
                                    <Route exact path='/dashboard/newBlog' component={props => <NewBlog {...props} />} />
                                    <Route exact path='/dashboard' component={props => <ReportComponent {...props} />} />
                                </Switch>
                            </Col>
                        </Row>
                    </div>
                </Row>
                <Row className='container-fulid footerWrapper'>
                    <Col md={12} sm={12}>
                        <FooterComponent />
                    </Col>
                </Row>
            </Fragment>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        authToken: state.auth.authToken
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setAauthToken: (token, id) => dispatch(authTokenAccess(token)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);
